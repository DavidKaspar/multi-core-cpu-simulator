import {Down, Up} from '../code'
import {createMachine} from '../machine'

export default createMachine(
    'Challenge - Swap Each Two',
    'Take pair of input values and output then with reverse order in each pair.',
    1,
    1,
    [
        {yIndex: 0, xIndex: 0, code: ``},
    ], [
        {yIndex: 0, xIndex: 0, port: Up, values: [4, 2, 100, 50]}
    ], [
        {yIndex: 0, xIndex: 0, port: Down, expectedValues: [2, 4, 50, 100]}
    ]
)
