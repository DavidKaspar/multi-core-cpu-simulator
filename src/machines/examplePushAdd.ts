import {createMachine} from '../machine'

const codeCounter = `
  start:
    add 1
    mov acc, down
`

const codePassthroughUpDown = `
  start:
    mov up, down
`

const codeCollect = `
  start:
    add up
`

export default createMachine(
    'Example - Push Add',
    'Example of counter and sum.',
    3,
    1,
    [
        {yIndex: 0, xIndex: 1, code: codeCounter},
        {yIndex: 1, xIndex: 1, code: codePassthroughUpDown},
        {yIndex: 2, xIndex: 1, code: codeCollect}
    ],
    [],
    []
)
