import challengeBinarySort from './challengeBinarySort'
import challengeFilterByAverage from './challengeFilterByAverage'
import challengeSwapTwo from './challengeSwapTwo'
import exampleInOutPassthrough from './exampleInOutPassthrough'
import examplePushAdd from "./examplePushAdd";
import exampleRandomAverage from "./exampleRandomAverage";
import exampleRoundAdd from "./exampleCircleAdd";

const machines = [
    challengeBinarySort,
    challengeFilterByAverage,
    challengeSwapTwo,
    exampleInOutPassthrough,
    examplePushAdd,
    exampleRandomAverage,
    exampleRoundAdd
]

export default machines
