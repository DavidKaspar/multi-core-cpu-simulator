import {Down, Left, Up} from '../code'
import {createMachine} from '../machine'

export default createMachine(
    'Challenge - Filter By Average',
    'Read 0-0 LEFT input until Not-a-Number and output values to 0-1 UP resp. 0-1 DOWN if they are greater or equal resp. less than the average of the input value.',
    1,
    2,
    [],
    [
        {yIndex: 0, xIndex: 0, port: Left, values: [4, 2, 70, 25, 1, 99, 50, NaN]}
    ],
    [
        {yIndex: 0, xIndex: 1, port: Up, expectedValues: [70, 99, 50]},
        {yIndex: 0, xIndex: 1, port: Down, expectedValues: [4, 2, 25, 1]}
    ]
)
