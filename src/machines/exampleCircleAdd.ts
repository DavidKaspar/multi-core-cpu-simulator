import {createMachine} from '../machine'

const codeAddSendReceive = `
  add 1
  mov acc, down
  mov down, acc
`

const codeRetrieveAddSendRetrieveAddSend = `
  mov up, acc
  add 1
  mov acc, down
  mov down, acc
  add 1
  mov acc, up
`

const codeRetrieveAddSend = `
  mov up, acc
  add 1
  mov acc, up
`

export default createMachine(
    'Example - Circle Add',
    'Example of passing and adding token through in and out ports.',
    3,
    1,
    [
        {yIndex: 0, xIndex: 0, code: codeAddSendReceive},
        {yIndex: 1, xIndex: 0, code: codeRetrieveAddSendRetrieveAddSend},
        {yIndex: 2, xIndex: 0, code: codeRetrieveAddSend}
    ],
    [],
    []
)
