import {createMachine} from '../machine'

const positiveGenerator = `
start:
  rndi 100
  mov acc, down
  jmp start
`

const comparator = `
start:
  mov up, acc
  mov down, bck
  sub bck
  mov acc, right
  jmp start
`

const negativeGenerator = `
start:
  rndi 100
  mov acc, up
  jmp start
`

const actualAverage = `
  push nan
loop:
  mov down, acc
  pop bck
  push acc
  jmp loop
`

const calculateAverage = `
start:
  push left
  mov 1, acc
  add bck
  mov acc, bck
  pop acc
  divi bck
  mov acc, up
  mov acc, down
  jmp start
`

const stackAverage = `
loop:
  push up
  jmp loop
`

export default createMachine(
    'Example - Calculate Average',
    'Example of difference comparison of two random generators.',
    3,
    2,
    [
        {yIndex: 0, xIndex: 0, code: positiveGenerator},
        {yIndex: 1, xIndex: 0, code: comparator},
        {yIndex: 2, xIndex: 0, code: negativeGenerator},
        {yIndex: 0, xIndex: 1, code: actualAverage},
        {yIndex: 1, xIndex: 1, code: calculateAverage},
        {yIndex: 2, xIndex: 1, code: stackAverage},
    ],
    [],
    []
)
