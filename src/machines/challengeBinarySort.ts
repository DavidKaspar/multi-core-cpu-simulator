import {Down, Up} from '../code'
import {createMachine} from '../machine'

export default createMachine(
    'Challenge - Binary Sort',
    'Read decimal number (from 0 to 255) from 0-1 UP input and write them to 0-1 DOWN sorted ascendently from the smallest to the biggest.',
    1,
    3,
    [],
    [
        {yIndex: 0, xIndex: 1, port: Up, values: [4, 2, 70, 25, 1, 99, 50, 186, 3, 65, 94, NaN]}
    ],
    [
        {yIndex: 0, xIndex: 1, port: Down, expectedValues: [1, 2, 3, 4, 25, 50, 65, 70, 94, 99, 186]}
    ]
)
