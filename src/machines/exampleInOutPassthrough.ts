import {Down, Up} from '../code'
import {createMachine} from '../machine'

const codePassthroughUpDown = `
  start:
    mov up, down
    jmp start
`

export default createMachine(
    'Example - In Out Passthrough',
    'Example of reading input queue and writing it to output queue.',
    1,
    1,
    [
        {yIndex: 0, xIndex: 0, code: codePassthroughUpDown},
    ], [
        {yIndex: 0, xIndex: 0, port: Up, values: [1, 10, 100]}
    ], [
        {yIndex: 0, xIndex: 0, port: Down, expectedValues: [1, 10, 100]}
    ]
)
