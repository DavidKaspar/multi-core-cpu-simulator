import React, {useEffect, useRef} from 'react'
import {select} from 'd3'

import {Machine} from './machine'
import {CpuStatus} from './cpu'

const cpuStatHeight = 16
const cpuStatWidth = 4
const cpuColorMap: Record<CpuStatus,string> = {
    'unused': 'lightgrey',
    'error': '#dd8080',
    'waiting': '#80dddd',
    'running': 'limegreen'
}

const MachineHistoryView: React.FC<{machine: Machine}> = ({machine}) => {
    const svgRef = useRef(null)

    useEffect(() => {
        const svg = select(svgRef.current)
        const allItems = machine.cpus.flatMap((cpu, cpuIndex) =>
            cpu.history.map((item, tickIndex) => ({
                ...item,
                cpuIndex,
                tickIndex
            }))
        )

        svg
            .selectAll('rect')
            .data(allItems)
            .join('rect')
            .attr('y', value => value.cpuIndex * cpuStatHeight)
            .attr('x', value => value.tickIndex * cpuStatWidth)
            .attr('height', cpuStatHeight)
            .attr('width', cpuStatWidth + 1)
            .attr('fill', value => cpuColorMap[value.status])
            .attr('stroke', 'white')
    }, [machine]);

    return <svg className="machine_history" ref={svgRef} height={machine.cpus.length * cpuStatHeight} width={machine.tickCount * cpuStatWidth}/>
}

export default MachineHistoryView
