import React from 'react'

const Value: React.FC<{ pre?: string, post?: string, value?: number, clsName?: string }> = ({
    pre,
    post,
    value,
    clsName
}) =>
    <span className={`value ${clsName !== undefined ? clsName : value !== undefined ? "value_normal" : "value_empty"}`}>
    {pre ? `  ${pre} ` : ''}<span
        className="value_holder">{value !== undefined ? value.toString() : '-'}</span>{post ? ` ${post}  ` : ''}
  </span>

export default Value
