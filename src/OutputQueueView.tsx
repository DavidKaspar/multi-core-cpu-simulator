import React from 'react'
import {Space} from 'antd'

import {OutputQueue} from './machine'
import Value from './Value'
import {range} from './utils'

const OutputQueueValue: React.FC<{ expectedValue?: number, actualValue?: number }> = ({
    expectedValue,
    actualValue
}) => {
    if (expectedValue !== undefined) {
        if (actualValue !== undefined) {
            if (expectedValue === actualValue) {
                return <Value pre="✓" value={expectedValue} clsName="value_output_queue_correct"/>
            } else {
                return <>
                    <Value pre="𐄂" value={actualValue} clsName="value_output_queue_incorrect"/>
                    <span> instead </span>
                    <Value pre="𐄂" value={expectedValue} clsName="value_output_queue_not_written"/>
                </>
            }
        } else {
            return <Value pre="·" value={expectedValue} clsName="value_output_queue_not_written"/>
        }
    } else {
        return <Value pre="𐄂" value={actualValue} clsName="value_output_queue_incorrect"/>
    }
}

const OutputQueueView: React.FC<{ output: OutputQueue }> = ({output}) => {
    return (
        <Space>
            {range(Math.max(output.expectedValues.length, output.actualValues.length)).map((index: number) =>
                <OutputQueueValue
                    key={index}
                    expectedValue={index < output.expectedValues.length ? output.expectedValues[index] : undefined}
                    actualValue={index < output.actualValues.length ? output.actualValues[index] : undefined}
                />
            )}
        </Space>
    )
}

export default OutputQueueView
