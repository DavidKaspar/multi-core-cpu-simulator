import {atom} from "recoil"

import {Machine} from "./machine"
import machine from "./machines/challengeFilterByAverage"

export const machineState = atom<Machine>({
    key: 'machineState',
    default: machine
})
