import React from 'react'

import './App.css'
import MachineView from './MachineView'

const App: React.FC = () => {
    return (
        <div className="App">
            <MachineView/>
        </div>
    )
}

export default App
