import React, {ChangeEvent, useState} from 'react'
import {Button, Col, Input, notification, Row, Space} from 'antd'

import {Cpu} from './cpu'
import {Down, Left, Right, Up} from './code'
import CpuStateValue from './Value'

const {TextArea} = Input

type CodeLine = {
    code: string
    actual: boolean
}

const codeToCodeLines = ({ip, labels, instructions}: Cpu): CodeLine[] => {
    const lines = []
    for (let i = 0; i < instructions.length; i++) {
        Object.getOwnPropertyNames(labels).filter(key => labels[key] === i).sort()
            .forEach(label => lines.push({
                code: `${label}:`,
                actual: false
            }))
        lines.push({
            code: `${i.toString().padStart(3, ' ')}  ${instructions[i].name()}`,
            actual: i === ip
        })
    }
    Object.getOwnPropertyNames(labels).filter(key => labels[key] >= instructions.length).sort()
        .forEach(label => lines.push({
            code: `${label}:`,
            actual: false
        }))
    return lines
}

const CpuStateRow: React.FC<{label?: string}> = ({label, children}) =>
    <Row className="cpu_state">
        <Col xs={3}>{label ? `${label}:` : undefined}</Col>
        <Col xs={21}>
            <Space wrap>
                {children}
            </Space>
        </Col>
    </Row>

const CpuView: React.FC<{ cpu: Cpu, onCpuCodeChange: (code: string) => string | undefined }> = ({cpu, onCpuCodeChange}) => {
    const [codeEditorOpened, setCodeEditorOpened] = useState(false)
    const [code, setCode] = useState('')

    const handleOpenCodeEditor = () => {
        setCode(cpu.code || '')
        setCodeEditorOpened(true)
    }

    const handleCloseCodeEditor = () => {
        setCode('')
        setCodeEditorOpened(false)
    }

    const handleCodeChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
        setCode(event.target.value)
    }

    const handleSaveCode = () => {
        const error = onCpuCodeChange(code)
        if (error === undefined) {
            setCodeEditorOpened(false)
        } else {
            notification['error']({
                message: 'Failed to change CPU code',
                description: error,
                duration: 2
            })
        }
    }

    let cpuState
    if (cpu.instructions.length === 0) {
        cpuState = "unused"
    } else if (cpu.ip < 0 || cpu.ip >= cpu.instructions.length) {
        cpuState = "error"
    } else if (cpu.waiting) {
        cpuState = "waiting"
    } else {
        cpuState = "running"
    }

    return (
        <div
            className={`cpu cpu_${cpuState}`}>
            <CpuStateRow label="In">
                <CpuStateValue pre={Left.sign()} value={cpu.inLeft}/>
                <CpuStateValue pre={Up.sign()} value={cpu.inUp}/>
                <CpuStateValue pre={Down.sign()} value={cpu.inDown}/>
                <CpuStateValue pre={Right.sign()} value={cpu.inRight}/>
            </CpuStateRow>
            <CpuStateRow label="Regs">
                <CpuStateValue pre="IP" value={cpu.ip}/>
                <CpuStateValue pre="ACC" value={cpu.acc}/>
                <CpuStateValue pre="BCK" value={cpu.bck}/>
            </CpuStateRow>
            <CpuStateRow label="Stack">
                {cpu.stack.map((value, index) =>
                    <CpuStateValue key={index} pre="·" value={value}/>
                )}
                {cpu.stack.length === 0 ? "-" : null}
            </CpuStateRow>
            <Row className="code">
                <Col xs={24}>
                    {codeEditorOpened ? (
                        <TextArea rows={10} value={code} onChange={handleCodeChange}/>
                    ) : codeToCodeLines(cpu).map(line =>
                        <div key={`${line.code}_${line.actual}`} className={line.actual ? 'code_line_actual' : 'code_line_normal'}>{line.code}</div>
                    )}
                </Col>
            </Row>
            <CpuStateRow>
                {codeEditorOpened ? (
                    <>
                        <Button shape="round" onClick={handleCloseCodeEditor}>Cancel</Button>
                        <Button shape="round" type="primary" onClick={handleSaveCode}>Save</Button>
                    </>
                ) : (
                    <Button shape="round" type="primary" onClick={handleOpenCodeEditor}>Edit</Button>
                )}
            </CpuStateRow>
            <CpuStateRow label="Ticks">
                <CpuStateValue pre="Running" value={cpu.runningTickCount}/>
                <CpuStateValue pre="Waiting" value={cpu.waitingTickCount}/>
                <CpuStateValue pre="Error" value={cpu.errorsTickCount}/>
            </CpuStateRow>
            <CpuStateRow label="Out">
                <CpuStateValue pre={Left.sign()} value={cpu.outLeft}/>
                <CpuStateValue pre={Up.sign()} value={cpu.outUp}/>
                <CpuStateValue pre={Down.sign()} value={cpu.outDown}/>
                <CpuStateValue pre={Right.sign()} value={cpu.outRight}/>
            </CpuStateRow>
        </div>
    )
}

export default CpuView
