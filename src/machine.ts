import {Cpu, CpuState, createCpu, PropagationPort, resetCpu, tickCpu} from './cpu';
import {Down, Left, Right, Up} from './code';

export type InputQueue = {
    yIndex: number
    xIndex: number
    port: PropagationPort
    values: number[]
    readValues: number
}

export interface OutputQueue {
    yIndex: number
    xIndex: number
    port: PropagationPort
    expectedValues: number[]
    actualValues: number[]
}

export type Machine = {
    title: string,
    description: string,
    height: number
    width: number
    tickCount: number
    cpus: Cpu[]
    inputs: InputQueue[]
    outputs: OutputQueue[]
}

const createInputQueue = (yIndex: number, xIndex: number, port: PropagationPort, values: number[]): InputQueue => ({
    yIndex,
    xIndex,
    port,
    values,
    readValues: 0
})

const resetInputQueue = (input: InputQueue): InputQueue =>
    createInputQueue(input.yIndex, input.xIndex, input.port, input.values)

const createOutputQueue = (yIndex: number, xIndex: number, port: PropagationPort, expectedValues: number[]): OutputQueue => ({
    yIndex,
    xIndex,
    port,
    expectedValues,
    actualValues: []
})

const resetOutputQueue = (output: OutputQueue): OutputQueue =>
    createOutputQueue(output.yIndex, output.xIndex, output.port, output.expectedValues)

export const createMachine = (
    title: string,
    description: string,
    height: number,
    width: number,
    cpuCodePatches: { yIndex: number, xIndex: number, code: string }[],
    inputQueuePatches: { yIndex: number, xIndex: number, port: PropagationPort, values: number[] }[],
    outputQueuePatches: { yIndex: number, xIndex: number, port: PropagationPort, expectedValues: number[] }[],
): Machine => {
    const cpus: Cpu[] = []
    for (let y = 0; y < height; y++) {
        for (let x = 0; x < width; x++) {
            cpus.push(createCpu(
                cpuCodePatches.find((cpuCodePatch) => y === cpuCodePatch.yIndex && x === cpuCodePatch.xIndex)?.code
            ))
        }
    }
    const inputs = inputQueuePatches.map(iq => createInputQueue(iq.yIndex, iq.xIndex, iq.port, iq.values))
    const outputs = outputQueuePatches.map(oq => createOutputQueue(oq.yIndex, oq.xIndex, oq.port, oq.expectedValues))
    return {
        title,
        description,
        height,
        width,
        tickCount: 0,
        cpus,
        inputs,
        outputs
    }
}

const propagateNeighbourToThis = (
    neighbour: Cpu,
    neighbourPort: PropagationPort,
    thisCpu: Cpu,
    thisCpuPort: PropagationPort
): Cpu => {
    const ni = neighbourPort.getInValue(neighbour)
    const no = neighbourPort.getOutValue(neighbour)
    const ti = thisCpuPort.getInValue(thisCpu)
    const to = thisCpuPort.getOutValue(thisCpu)

    let cs = thisCpu as CpuState
    cs = thisCpuPort.setInValue(cs, no !== undefined && ti === undefined ? no : ti)
    cs = thisCpuPort.setOutValue(cs, to !== undefined && ni === undefined ? undefined : to)

    return {
        ...thisCpu,
        ...cs
    }
}

const propagateInOutQueue = (
    inputs: InputQueue[],
    outputs: OutputQueue[],
    thisCpu: Cpu,
    yIndex: number,
    xIndex: number,
    thisCpuPort: PropagationPort
): [InputQueue[], OutputQueue[], Cpu] => {
    let cs = thisCpu as CpuState

    const i = inputs.map(input => {
        if (input.yIndex === yIndex && input.xIndex === xIndex && input.port === thisCpuPort) {
            if (thisCpuPort.getInValue(cs) === undefined && input.readValues < input.values.length) {
                cs = thisCpuPort.setInValue(cs, input.values[input.readValues])
                return {
                    ...input,
                    readValues: input.readValues + 1
                }
            }
        }
        return input
    })

    const o = outputs.map(output => {
        if (output.yIndex === yIndex && output.xIndex === xIndex && output.port === thisCpuPort) {
            const out = thisCpuPort.getOutValue(cs)
            if (out !== undefined) {
                cs = thisCpuPort.setOutValue(cs, undefined)
                return {
                    ...output,
                    actualValues: [...output.actualValues, out]
                }
            }
        }
        return output
    })

    const cpu = {
        ...thisCpu,
        ...cs,
    }
    return [i, o, cpu]
}

export const tickMachine = (machine: Machine): Machine => {
    const tickedCpus = machine.cpus.map(cpu => tickCpu(cpu))

    const tickedCpuAt = (yIndex: number, xIndex: number): Cpu =>
        tickedCpus[yIndex * machine.width + xIndex]

    let inputs = machine.inputs
    let outputs = machine.outputs
    const cpus: Cpu[] = []

    for (let y = 0; y < machine.height; y++) {
        for (let x = 0; x < machine.width; x++) {
            let cpu = tickedCpuAt(y, x)
            if (y > 0) {
                cpu = propagateNeighbourToThis(tickedCpuAt(y - 1, x), Down, cpu, Up)
            } else {
                const [i, o, c] = propagateInOutQueue(inputs, outputs, cpu, y, x, Up)
                inputs = i
                outputs = o
                cpu = c
            }
            if (y < machine.height - 1) {
                cpu = propagateNeighbourToThis(tickedCpuAt(y + 1, x), Up, cpu, Down)
            } else {
                const [i, o, c] = propagateInOutQueue(inputs, outputs, cpu, y, x, Down)
                inputs = i
                outputs = o
                cpu = c
            }
            if (x > 0) {
                cpu = propagateNeighbourToThis(tickedCpuAt(y, x - 1), Right, cpu, Left)
            } else {
                const [i, o, c] = propagateInOutQueue(inputs, outputs, cpu, y, x, Left)
                inputs = i
                outputs = o
                cpu = c
            }
            if (x < machine.width - 1) {
                cpu = propagateNeighbourToThis(tickedCpuAt(y, x + 1), Left, cpu, Right)
            } else {
                const [i, o, c] = propagateInOutQueue(inputs, outputs, cpu, y, x, Right)
                inputs = i
                outputs = o
                cpu = c
            }
            cpus.push(cpu)
        }
    }
    return {
        ...machine,
        tickCount: machine.tickCount + 1,
        cpus,
        inputs,
        outputs,
    }
}

export const resetMachine = (machine: Machine): Machine => ({
    ...machine,
    tickCount: 0,
    cpus: machine.cpus.map(cpu => resetCpu(cpu)),
    inputs: machine.inputs.map(input => resetInputQueue(input)),
    outputs: machine.outputs.map(output => resetOutputQueue(output)),
})

export const changeMachineCpuCode = (machine: Machine, cpuIndex: number, code: string): [Machine, string | undefined] => {
    try {
        const newCpus = [...machine.cpus]
        newCpus.splice(cpuIndex, 1, createCpu(code))
        const newMachine = {
            ...machine,
            cpus: newCpus
        }
        return [
            resetMachine(newMachine),
            undefined
        ]
    } catch (e) {
        console.log(e)
        return [machine, e.message]
    }
}
