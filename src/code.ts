import {Code, Cpu, CpuState, InstrDst, InstrSrc, Instruction, PropagationPort} from './cpu'

export const Up: InstrSrc & InstrDst & PropagationPort = {
    name: () => 'UP',
    sign: () => '⬆',
    canRetrieve: (state: CpuState): boolean => state.inUp !== undefined,
    retrieve: (state: CpuState): { state: CpuState, value: number } => ({
        state: {...state, inUp: undefined},
        value: state.inUp!!
    }),
    canSend: (state: CpuState): boolean => state.outUp === undefined,
    send: (state: CpuState, value: number): CpuState => ({...state, outUp: value}),
    getInValue: (state: CpuState): number | undefined => state.inUp,
    setInValue: (state: CpuState, value: number | undefined): CpuState => ({...state, inUp: value}),
    getOutValue: (state: CpuState): number | undefined => state.outUp,
    setOutValue: (state: CpuState, value: number | undefined): CpuState => ({...state, outUp: value})
}

export const Down: InstrSrc & InstrDst & PropagationPort = {
    name: () => 'DOWN',
    sign: () => '⬇',
    canRetrieve: (state: CpuState): boolean => state.inDown !== undefined,
    retrieve: (state: CpuState): { state: CpuState, value: number } => ({
        state: {...state, inDown: undefined},
        value: state.inDown!!
    }),
    canSend: (state: CpuState): boolean => state.outDown === undefined,
    send: (state: CpuState, value: number): CpuState => ({...state, outDown: value}),
    getInValue: (state: CpuState): number | undefined => state.inDown,
    setInValue: (state: CpuState, value: number | undefined): CpuState => ({...state, inDown: value}),
    getOutValue: (state: CpuState): number | undefined => state.outDown,
    setOutValue: (state: CpuState, value: number | undefined): CpuState => ({...state, outDown: value})
}

export const Left: InstrSrc & InstrDst & PropagationPort = {
    name: () => 'LEFT',
    sign: () => '⬅',
    canRetrieve: (state: CpuState): boolean => state.inLeft !== undefined,
    retrieve: (state: CpuState): { state: CpuState, value: number } => ({
        state: {...state, inLeft: undefined},
        value: state.inLeft!!
    }),
    canSend: (state: CpuState): boolean => state.outLeft === undefined,
    send: (state: CpuState, value: number): CpuState => ({...state, outLeft: value}),
    getInValue: (state: CpuState): number | undefined => state.inLeft,
    setInValue: (state: CpuState, value: number | undefined): CpuState => ({...state, inLeft: value}),
    getOutValue: (state: CpuState): number | undefined => state.outLeft,
    setOutValue: (state: CpuState, value: number | undefined): CpuState => ({...state, outLeft: value})
}

export const Right: InstrSrc & InstrDst & PropagationPort = {
    name: () => 'RIGHT',
    sign: () => '⮕',
    canRetrieve: (state: CpuState): boolean => state.inRight !== undefined,
    retrieve: (state: CpuState): { state: CpuState, value: number } => ({
        state: {...state, inRight: undefined},
        value: state.inRight!!
    }),
    canSend: (state: CpuState): boolean => state.outRight === undefined,
    send: (state: CpuState, value: number): CpuState => ({...state, outRight: value}),
    getInValue: (state: CpuState): number | undefined => state.inRight,
    setInValue: (state: CpuState, value: number | undefined): CpuState => ({...state, inRight: value}),
    getOutValue: (state: CpuState): number | undefined => state.outRight,
    setOutValue: (state: CpuState, value: number | undefined): CpuState => ({...state, outRight: value})
}

const Acc: InstrSrc & InstrDst = {
    name: () => 'ACC',
    canRetrieve: (_: CpuState): boolean => true,
    retrieve: (state: CpuState): { state: CpuState, value: number } => ({state, value: state.acc}),
    canSend: (_: CpuState): boolean => true,
    send: (state: CpuState, value: number): CpuState => ({...state, acc: value})
}

const Bck: InstrSrc & InstrDst = {
    name: () => 'BCK',
    canRetrieve: (_: CpuState): boolean => true,
    retrieve: (state: CpuState): { state: CpuState, value: number } => ({state, value: state.bck}),
    canSend: (_: CpuState): boolean => true,
    send: (state: CpuState, value: number): CpuState => ({...state, bck: value})
}

const Ip: InstrSrc & InstrDst = {
    name: () => 'IP',
    canRetrieve: (_: CpuState): boolean => true,
    retrieve: (state: CpuState): { state: CpuState, value: number } => ({state, value: state.ip}),
    canSend: (_: CpuState): boolean => true,
    send: (state: CpuState, value: number): CpuState => ({...state, ip: value})
}

const Constant = (constant: number): InstrSrc => ({
    name: () => constant.toString(),
    canRetrieve: (_: CpuState): boolean => true,
    retrieve: (state: CpuState): { state: CpuState, value: number } => ({state, value: constant}),
})

const Mov = (src: InstrSrc, dst: InstrDst): Instruction => ({
    name: () => `MOV ${src.name()}, ${dst.name()}`,
    execute: (cpu: Cpu): Cpu => {
        if (src.canRetrieve(cpu) && dst.canSend(cpu)) {
            const {state, value} = src.retrieve(cpu)
            const newState = dst.send(state, value)
            return {
                ...cpu,
                ...newState,
                waiting: false,
                ip: newState.ip + 1
            }
        }
        return {
            ...cpu,
            waiting: true
        }
    }
})

const Unary = (name: string, calculate: (first: number) => number): Instruction => ({
    name: () => `${name}`,
    execute: (cpu: Cpu): Cpu => {
        return {
            ...cpu,
            acc: calculate(cpu.acc),
            waiting: false,
            ip: cpu.ip + 1
        }
    }
})

const Binary = (name: string, calculate: (first: number, second: number) => number, src: InstrSrc): Instruction => ({
    name: () => `${name} ${src.name()}`,
    execute: (cpu: Cpu): Cpu => {
        if (src.canRetrieve(cpu)) {
            const {state, value} = src.retrieve(cpu)
            return {
                ...cpu,
                ...state,
                acc: calculate(state.acc, value),
                waiting: false,
                ip: state.ip + 1
            }
        }
        return {
            ...cpu,
            waiting: true
        }
    }
})

const Push = (src: InstrSrc): Instruction => ({
    name: () => `PUSH ${src.name()}`,
    execute: (cpu: Cpu): Cpu => {
        if (src.canRetrieve(cpu)) {
            const {state, value} = src.retrieve(cpu)
            const newState = {
                ...state,
                stack: [value, ...state.stack]
            }
            return {
                ...cpu,
                ...newState,
                waiting: false,
                ip: newState.ip + 1
            }
        }
        return {
            ...cpu,
            waiting: true
        }
    }
})

const Pop = (dst: InstrDst): Instruction => ({
    name: () => `POP ${dst.name()}`,
    execute: (cpu: Cpu): Cpu => {
        if (dst.canSend(cpu)) {
            if (cpu.stack.length === 0) {
                return {
                    ...cpu,
                    waiting: false,
                    ip: -1
                }
            }
            const [value, ...stack] = cpu.stack
            const poppedState = {
                ...cpu,
                stack
            }
            const newState = dst.send(poppedState, value)
            return {
                ...cpu,
                ...newState,
                waiting: false,
                ip: newState.ip + 1
            }
        }
        return {
            ...cpu,
            waiting: true
        }
    }
})

const Call = (label: string): Instruction => ({
    name: () => `CALL ${label}`,
    execute: (cpu: Cpu): Cpu => ({
        ...cpu,
        waiting: false,
        ip: cpu.labels[label]!!,
        stack: [cpu.ip + 1, ...cpu.stack]
    })
})

const Return = (): Instruction => ({
    name: () => `RET`,
    execute: (cpu: Cpu): Cpu => {
        if (cpu.stack.length === 0) {
            return {
                ...cpu,
                waiting: false,
                ip: -1
            }
        }
        const [ip, ...stack] = cpu.stack
        return {
            ...cpu,
            waiting: false,
            ip,
            stack
        }
    }
})

const Jump = (name: string, cond: (acc: number) => boolean, label: string): Instruction => ({
    name: () => `${name} ${label}`,
    execute: (cpu: Cpu): Cpu => ({
        ...cpu,
        waiting: false,
        ip: cond(cpu.acc) ? cpu.labels[label]!! : cpu.ip + 1
    })
})

const Halt = () => ({
    name: () => 'HALT',
    execute: (cpu: Cpu): Cpu => ({
        ...cpu,
        waiting: true,
    })
})

class DecodeInstrLineError extends Error {
    constructor(instrLine: String) {
        super(`Invalid instr line: ${instrLine}`)
        this.name = 'DecodeInstrLineError'
    }
}

const decodeInstrLine = (line: String): {
    name: string,
    args: string[]
} => {
    const spaceIndex = line.indexOf(' ')
    if (spaceIndex < 0) {
        throw new DecodeInstrLineError(line)
    }
    const name = line.substring(0, spaceIndex).trim()
    const args =
        spaceIndex < line.length
            ? line
                .substring(spaceIndex + 1)
                .trim()
                .split(',')
                .map(arg => arg.trim())
            : []
    return {
        name,
        args
    }
}

class DecodeInstrSrcError extends Error {
    constructor(instrSrc: String) {
        super(`Invalid instr src: ${instrSrc}`)
        this.name = 'DecodeInstrSrcError'
    }
}

const decodeInstrSrc = (src: string): InstrSrc => {
    switch (src) {
        case 'ACC':
            return Acc
        case 'BCK':
            return Bck
        case 'IP':
            return Ip
        case 'UP':
            return Up
        case 'DOWN':
            return Down
        case 'LEFT':
            return Left
        case 'RIGHT':
            return Right
    }
    if (src === 'NAN') {
        return Constant(NaN)
    }
    const num = Number.parseInt(src)
    if (isNaN(num)) {
        throw new DecodeInstrSrcError(src)
    }
    return Constant(num)
}

class DecodeInstrDstError extends Error {
    constructor(instrDst: String) {
        super(`Invalid instr dst: ${instrDst}`)
        this.name = 'DecodeInstrDstError'
    }
}

const decodeInstrDst = (dst: string): InstrDst => {
    switch (dst) {
        case 'ACC':
            return Acc
        case 'BCK':
            return Bck
        case 'IP':
            return Ip
        case 'UP':
            return Up
        case 'DOWN':
            return Down
        case 'LEFT':
            return Left
        case 'RIGHT':
            return Right
    }
    throw new DecodeInstrDstError(dst)
}

class DecodeCodeLineError extends Error {
    constructor(codeLine: String) {
        super(`Invalid code line: ${codeLine}`)
        this.name = 'DecodeCodeLineError'
    }
}

class DecodeMissingLabelError extends Error {
    constructor(missingLabel: String) {
        super(`Missing label: ${missingLabel}`)
        this.name = 'DecodeMissingLabelError'
    }
}

export const compile = (code?: string): Code => {
    const lines = code?.toUpperCase().split('\n') || []
    const labels: Record<string, number> = {}
    const requiredLabels = new Set<string>()
    const instructions: Instruction[] = []
    lines.forEach(line => {
        const l = line.trim()
        if (l === '' || l.startsWith('#')) {
        } else if (l.endsWith(':')) {
            const label = l.substring(0, l.length - ':'.length).trim()
            if (!label.match("^[A-Za-z][A-Za-z0-9_]*$")) {
                throw new DecodeInstrLineError(line)
            }
            labels[label] = instructions.length
        } else if (line === 'NEG') {
            instructions.push(Unary(
                line,
                (first) => -first
            ))
        } else if (line === 'NOT') {
            instructions.push(Unary(
                line,
                (first) => first !== 0 ? 0 : 1
            ))
        } else if (line === 'FLOOR') {
            instructions.push(Unary(
                line,
                (first) => Math.floor(first)
            ))
        } else if (line === 'ROUND') {
            instructions.push(Unary(
                line,
                (first) => Math.round(first)
            ))
        } else if (line === 'CEIL') {
            instructions.push(Unary(
                line,
                (first) => Math.ceil(first)
            ))
        } else if (line === 'RETURN') {
            instructions.push(Return(
            ))
        } else if (line === 'HALT') {
            instructions.push(Halt(
            ))
        } else {
            const codeLine = decodeInstrLine(l)

            if (codeLine.name === 'MOV' && codeLine.args.length === 2) {
                instructions.push(Mov(
                    decodeInstrSrc(codeLine.args[0]),
                    decodeInstrDst(codeLine.args[1])
                ))
            } else if (codeLine.name === 'ADD' && codeLine.args.length === 1) {
                instructions.push(Binary(
                    codeLine.name,
                    (first, second) => first + second,
                    decodeInstrSrc(codeLine.args[0])
                ))
            } else if (codeLine.name === 'SUB' && codeLine.args.length === 1) {
                instructions.push(Binary(
                    codeLine.name,
                    (first, second) => first - second,
                    decodeInstrSrc(codeLine.args[0])
                ))
            } else if (codeLine.name === 'MUL' && codeLine.args.length === 1) {
                instructions.push(Binary(
                    codeLine.name,
                    (first, second) => first * second,
                    decodeInstrSrc(codeLine.args[0])
                ))
            } else if (codeLine.name === 'DIV' && codeLine.args.length === 1) {
                instructions.push(Binary(
                    codeLine.name,
                    (first, second) => first / second,
                    decodeInstrSrc(codeLine.args[0])
                ))
            } else if (codeLine.name === 'DIVI' && codeLine.args.length === 1) {
                instructions.push(Binary(
                    codeLine.name,
                    (first, second) => Math.floor(first / second),
                    decodeInstrSrc(codeLine.args[0])
                ))
            } else if (codeLine.name === 'MOD' && codeLine.args.length === 1) {
                instructions.push(Binary(
                    codeLine.name,
                    (first, second) => first % second,
                    decodeInstrSrc(codeLine.args[0])
                ))
            } else if (codeLine.name === 'RND' && codeLine.args.length === 1) {
                instructions.push(Binary(
                    codeLine.name,
                    (first, second) => Math.random() * second,
                    decodeInstrSrc(codeLine.args[0])
                ))
            } else if (codeLine.name === 'RNDI' && codeLine.args.length === 1) {
                instructions.push(Binary(
                    codeLine.name,
                    (first, second) => Math.floor(Math.random() * second),
                    decodeInstrSrc(codeLine.args[0])
                ))
            } else if (codeLine.name === 'AND' && codeLine.args.length === 1) {
                instructions.push(Binary(
                    codeLine.name,
                    (first, second) => first & second,
                    decodeInstrSrc(codeLine.args[0])
                ))
            } else if (codeLine.name === 'OR' && codeLine.args.length === 1) {
                instructions.push(Binary(
                    codeLine.name,
                    (first, second) => first | second,
                    decodeInstrSrc(codeLine.args[0])
                ))
            } else if (codeLine.name === 'XOR' && codeLine.args.length === 1) {
                instructions.push(Binary(
                    codeLine.name,
                    (first, second) => first ^ second,
                    decodeInstrSrc(codeLine.args[0])
                ))
            } else if (codeLine.name === 'PUSH' && codeLine.args.length === 1) {
                instructions.push(Push(
                    decodeInstrSrc(codeLine.args[0])
                ))
            } else if (codeLine.name === 'POP' && codeLine.args.length === 1) {
                instructions.push(Pop(
                    decodeInstrDst(codeLine.args[0])
                ))
            } else if (codeLine.name === 'CALL' && codeLine.args.length === 1) {
                instructions.push(Call(
                    codeLine.args[0]
                ))
                requiredLabels.add(codeLine.args[0])
            } else if (codeLine.name === 'JMP' && codeLine.args.length === 1) {
                instructions.push(Jump(
                    codeLine.name,
                    (_) => true,
                    codeLine.args[0]
                ))
                requiredLabels.add(codeLine.args[0])
            } else if (codeLine.name === 'JZ' && codeLine.args.length === 1) {
                instructions.push(Jump(
                    codeLine.name,
                    (acc) => acc === 0,
                    codeLine.args[0]
                ))
                requiredLabels.add(codeLine.args[0])
            } else if (codeLine.name === 'JNZ' && codeLine.args.length === 1) {
                instructions.push(Jump(
                    codeLine.name,
                    (acc) => acc !== 0,
                    codeLine.args[0]
                ))
                requiredLabels.add(codeLine.args[0])
            } else if (codeLine.name === 'JG' && codeLine.args.length === 1) {
                instructions.push(Jump(
                    codeLine.name,
                    (acc) => acc > 0,
                    codeLine.args[0]
                ))
                requiredLabels.add(codeLine.args[0])
            } else if (codeLine.name === 'JGE' && codeLine.args.length === 1) {
                instructions.push(Jump(
                    codeLine.name,
                    (acc) => acc >= 0,
                    codeLine.args[0]
                ))
                requiredLabels.add(codeLine.args[0])
            } else if (codeLine.name === 'JL' && codeLine.args.length === 1) {
                instructions.push(Jump(
                    codeLine.name,
                    (acc) => acc < 0,
                    codeLine.args[0]
                ))
                requiredLabels.add(codeLine.args[0])
            } else if (codeLine.name === 'JLE' && codeLine.args.length === 1) {
                instructions.push(Jump(
                    codeLine.name,
                    (acc) => acc <= 0,
                    codeLine.args[0]
                ))
                requiredLabels.add(codeLine.args[0])
            } else if (codeLine.name === 'JNUM' && codeLine.args.length === 1) {
                instructions.push(Jump(
                    codeLine.name,
                    (acc) => !isNaN(acc),
                    codeLine.args[0]
                ))
                requiredLabels.add(codeLine.args[0])
            } else if (codeLine.name === 'JNAN' && codeLine.args.length === 1) {
                instructions.push(Jump(
                    codeLine.name,
                    (acc) => isNaN(acc),
                    codeLine.args[0]
                ))
                requiredLabels.add(codeLine.args[0])
            } else {
                throw new DecodeCodeLineError(line)
            }
        }
    })
    requiredLabels.forEach(label => {
        if (labels[label] === undefined) {
            throw new DecodeMissingLabelError(label)
        }
    })
    return {
        code,
        labels,
        instructions
    }
}
