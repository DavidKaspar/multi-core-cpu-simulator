import React, {useEffect, useState} from 'react'
import {useRecoilState} from 'recoil'
import {Button, Col, Row, Space} from 'antd'

import {machineState} from './machineState'
import {changeMachineCpuCode, Machine, resetMachine, tickMachine} from './machine'
import CpuView from './CpuView'
import InputQueueView from './InputQueueView'
import OutputQueueView from './OutputQueueView'
import {arrayEquals, range} from './utils'
import Value from "./Value";
import machines from './machines/machines'
import MachineHistoryView from "./MachineHistoryView";

const MachineBlockRow: React.FC<{label?: string, space?: boolean}> = ({label, space = true, children}) =>
    <Row className="machine_block">
        <Col xs={4}>{label ? `${label}:` : undefined}</Col>
        <Col xs={20} className={space ? '': 'machine_block_no_space'}>
            {space ? (
                <Space wrap>
                    {children}
                </Space>
            ) : children }
        </Col>
    </Row>

const MachineView: React.FC = () => {
    const [machine, setMachine] = useRecoilState(machineState)
    const [playing, setPlaying] = useState(false)
    const [machineSelectorOpened, setMachineSelectorOpened] = useState(false)

    const isWaitingInOutputQueue =
        machine.outputs.filter(output => output.actualValues.length < output.expectedValues.length).length > 0
    const isCompletedInOutputQueue =
        machine.outputs.filter(output => arrayEquals(output.actualValues, output.expectedValues)).length === machine.outputs.length

    useEffect(() => {
        const timeout = setTimeout(() => {
            let hasNoQueues = machine.outputs.length === 0 && machine.outputs.length === 0;
            if (playing && (isWaitingInOutputQueue || hasNoQueues)) {
                setMachine(tickMachine(machine))
            }
        }, 10)

        return () => clearTimeout(timeout)
    }, [machine, setMachine, playing, isWaitingInOutputQueue])

    const handlePlay = () => {
        setPlaying(true)
    }

    const handlePause = () => {
        setPlaying(false)
    }

    const handleStep = () => {
        setPlaying(false)
        setMachine(tickMachine(machine))
    }

    const handleReset = () => {
        setPlaying(false)
        setMachine(resetMachine(machine))
    }

    const handleOpenMachineSelector = () => {
        setMachineSelectorOpened(true)
    }

    const handleCloseMachineSelector = () => {
        setMachineSelectorOpened(false)
    }

    const handleMachineSelected = (selectedMachine: Machine) => {
        setMachine(resetMachine(selectedMachine))
        setMachineSelectorOpened(false)
    }

    const handleOpenHelp = () => {
        window.open('https://gitlab.com/DavidKaspar/multi-core-cpu-simulator/#simulator-guide', '_blank')
    }

    const handleCpuCodeChange: (cpuIndex: number, code: string) => string | undefined = (cpuIndex, code) => {
        const [newMachine, error] = changeMachineCpuCode(machine, cpuIndex, code)
        setMachine(newMachine)
        return error
    }

    return (
        <div>
            <div className="header header_buttons">
                <Space>
                    {playing ? (
                        <Button size="large" type="default" shape="round" disabled={!playing} onClick={handlePause}>Pause</Button>
                    ) : (
                        <Button size="large" type="primary" shape="round" disabled={playing} onClick={handlePlay}>Play</Button>
                    )}
                    <Button size="large" type="default" shape="round" onClick={handleStep}>Step</Button>
                    <Button size="large" type="default" shape="round" onClick={handleReset}>Reset</Button>
                    {machineSelectorOpened ? (
                        <Button size="large" type="primary" shape="round" onClick={handleCloseMachineSelector}>Close</Button>
                    ) : (
                        <Button size="large" type="default" shape="round" onClick={handleOpenMachineSelector}>Machines</Button>
                    )}
                    <Button size="large" type="default" shape="round" onClick={handleOpenHelp}>Help</Button>
                </Space>
            </div>
            {machineSelectorOpened ? (
                <div className="header header_listing">
                    {machines.map(theMachine => (
                        <Row key={theMachine.title} justify="center">
                            <Col xs={16}>
                                <Space align="baseline">
                                    <Button size="large" shape="round" onClick={() => handleMachineSelected(theMachine)}>Load</Button>
                                    <h2>{theMachine.title}</h2>
                                </Space>
                                <p>{theMachine.description}</p>
                            </Col>
                        </Row>
                    ))}
                </div>
            ) : null}
            <MachineBlockRow label="Description">
                <span>{machine.description}</span>
            </MachineBlockRow>
            <MachineBlockRow label="Status">
                <span className={`machine_description ${isWaitingInOutputQueue ? 'machine_description_waiting' : isCompletedInOutputQueue ? 'machine_description_correct' : 'machine_description_incorrect'}`}>
                    {isWaitingInOutputQueue ? 'waiting' : isCompletedInOutputQueue ? 'completed successfully with matching output' : 'failed to match expected output'}
                </span>
                <Value pre="Ticks" value={machine.tickCount}/>
            </MachineBlockRow>
            <MachineBlockRow label="Histogram" space={false}>
                <MachineHistoryView machine={machine}/>
            </MachineBlockRow>
            {machine.inputs.map((input, i) =>
                <MachineBlockRow key={i} label={`Input ${input.yIndex}-${input.xIndex} ${input.port.sign()}`}>
                    <InputQueueView input={input}/>
                </MachineBlockRow>
            )}
            {machine.outputs.map((output, i) =>
                <MachineBlockRow key={i} label={`Output ${output.yIndex}-${output.xIndex} ${output.port.sign()}`}>
                    <OutputQueueView output={output}/>
                </MachineBlockRow>
            )}
            <div className="machine_block">
                {range(machine.height).map(y =>
                    <Row key={y} wrap={false}>
                        {range(machine.width).map(x =>
                            <Col key={x} xs={8}>
                                <CpuView
                                    cpu={machine.cpus[y * machine.width + x]}
                                    onCpuCodeChange={(code) => handleCpuCodeChange(y * machine.width + x, code)}
                                />
                            </Col>
                        )}
                    </Row>
                )}
            </div>
        </div>
    )
}

export default MachineView
