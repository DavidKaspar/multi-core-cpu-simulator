export const range = (count: number): number[] =>
    // @ts-ignore
    [...Array(count).keys()]

export const arrayEquals = (a: any[], b: any[]) =>
    Array.isArray(a) &&
    Array.isArray(b) &&
    a.length === b.length &&
    a.every((val, index) => val === b[index])
