import {compile} from "./code";

export type CpuState = {
    waiting: boolean,
    ip: number,
    acc: number,
    bck: number,
    stack: number[],
    inUp?: number,
    inDown?: number,
    inLeft?: number,
    inRight?: number,
    outUp?: number,
    outDown?: number,
    outLeft?: number,
    outRight?: number,
}

export interface Instruction {
    name: () => string,
    execute: (cpu: Cpu) => Cpu
}

export type Code = {
    code?: string,
    labels: Record<string, number>
    instructions: Instruction[]
}

export type CpuStatus = 'unused' | 'running' | 'waiting' | 'error'

type CpuHistoryItem = {
    status: CpuStatus
}

export type CpuStat = {
    runningTickCount: number,
    waitingTickCount: number,
    errorsTickCount: number,
    history: CpuHistoryItem[]
}

export type Cpu = Code & CpuStat & CpuState

export interface InstrSrc {
    name: () => string,
    canRetrieve: (state: CpuState) => boolean
    retrieve: (state: CpuState) => { state: CpuState, value: number }
}

export interface InstrDst {
    name: () => string,
    canSend: (state: CpuState) => boolean
    send: (state: CpuState, value: number) => CpuState
}

export interface PropagationPort {
    name: () => string,
    sign: () => string,
    getInValue: (state: CpuState) => number | undefined
    setInValue: (state: CpuState, value: number | undefined) => CpuState
    getOutValue: (state: CpuState) => number | undefined
    setOutValue: (state: CpuState, value: number | undefined) => CpuState
}

export const createCpu = (code?: string): Cpu => ({
    ...compile(code),
    runningTickCount: 0,
    waitingTickCount: 0,
    errorsTickCount: 0,
    history: [],
    waiting: false,
    ip: 0,
    acc: 0,
    bck: 0,
    stack: []
})

export const resetCpu = (cpu: Cpu): Cpu =>
    createCpu(cpu.code)

const resetCpuNoInOut = (cpu: Cpu): Cpu => ({
    ...cpu,
    waiting: false,
    ip: 0,
    stack: []
})

export const tickCpu = (cpu: Cpu): Cpu => {
    if (cpu.instructions.length === 0) {
        return {
            ...cpu,
            history: [...cpu.history, {
                status: 'unused'
            }]
        }
    }
    const instruction = cpu.instructions[cpu.ip]
    if (instruction === undefined) {
        const newCpu = resetCpuNoInOut(cpu)
        return {
            ...newCpu,
            errorsTickCount: newCpu.errorsTickCount + 1,
            history: [...newCpu.history, {
                status: 'error'
            }]
        }

    }
    const newCpu = instruction.execute(cpu)
    return {
        ...newCpu,
        runningTickCount: newCpu.runningTickCount + (newCpu.waiting ? 0 : 1),
        waitingTickCount: newCpu.waitingTickCount + (newCpu.waiting ? 1 : 0),
        history: [...newCpu.history, {
            status: newCpu.waiting ? 'waiting' : 'running'
        }]
    }
}
