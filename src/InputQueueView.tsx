import React from 'react'
import {Space} from 'antd'

import {InputQueue} from './machine'
import Value from './Value'

const InputQueueView: React.FC<{ input: InputQueue }> = ({input}) => {
    return (
        <Space wrap={true}>
            {input.values.map((value: number, i: number) =>
                <Value
                    key={i} pre={i >= input.readValues ? "·" : "✓"} value={value}
                    clsName={i >= input.readValues ? "value_input_queue_not_read" : "value_input_queue_correct"}/>
            )}
        </Space>
    )
}

export default InputQueueView
