# Multi Core CPU Simulator

This is a simulator of a hypothetical CPU with multiple independently running CPUs in grid layout.

## Online Simulator

The simulator is available online on [Multi Core CPU Simulator](https://davidkaspar.gitlab.io/multi-core-cpu-simulator/) web-page.

## Simulator Guide

### Machine

Simulator runs as a single machine and has the following parts:
* CPUs
* Input Queues
* Output Queues

### CPU

CPUs are running independently and are layout in grid and connected with their neighbours on each side using their input and output ports.
Input and output ports of neighbour CPUs are connected via blocking queue that can hold up to single item in each direction.

CPU has:
* IP register - holds the pointer to actually executed instruction and increased by 1 after instruction execution
* ACC register - accumulator register
* BCK register - backup register
* stack - endless [LIFO](https://en.wikipedia.org/wiki/LIFO) queue of numbers
* up, down left, right - input and output ports

Edge ports (i.e. ports that are not having neighbour CPU) are unused and therefore CPUs cannot read from or write to them.

### CPU States

A CPU can be in the following states: 
* running - CPU is able to execute next instruction
* waiting - CPU is waiting for a blocking operation to be unblocked
* error - CPU executed invalid instruction or instruction cannot be found because IP register points out of the program

### Input Queue

Input Queues are representing input data for the machine and are expected to be read by the machine.

An Input Queue is implemented as blocking [FIFO](https://en.wikipedia.org/wiki/FIFO) queue of numbers that can be assigned to an input edge port.
When assigned, related CPU can read from the edge port.

### Output Queue

Output Queues are representing output data for the machine and are expected to be written by the machine.

An Output Queue is implemented as blocking [FIFO](https://en.wikipedia.org/wiki/FIFO) queue of numbers that can be assigned to an output edge port.
When assigned, related CPU can write to the edge port.

The Output Queue holds even queue of expected values that is used to compare actual and expected result of the machine execution.

### Compiler

CPUs are programmed via Assembly-like language. Language is case-insensitive. Each line of program can be one of:
* line comment
* label
* instruction 

Example:
```asm
# comment line
label:
MOV SRC, DST
JMP label
```

### Instruction Operands

SRC represents any data input:
* input ports: `UP`, `DOWN`, `LEFT`, `RIGHT`
* registers: `ACC`, `BCK`, `IP`
* `NaN` - not-a-number
* any number

Any instruction that wants to read from input port is automatically blocked until the input port has value available. Upon read from the input port, the value is popped from the port.

Upon reading from a register, the value copied.

DST represents any data output:
* output ports: `UP`, `DOWN`, `LEFT`, `RIGHT`
* registers: `ACC`, `BCK`, `IP`

Any instruction that wants to write to output port is automatically blocked until the input port has value available. Upon write to the output port, the value is pushed to the port.

### Instruction Set

Data transfer:
* `MOV SRC, DST` - moves value from SRC to DST

Stack operations:
* `PUSH SRC` - pushs value from SRC to CPU stack
* `POP DST` - pops value from CPU stack to DST

Arithmentic operations:
* `ADD SRC` - modifies ACC register by adding SRC to its value
* `SUB` - modifies ACC register by subtracting SRC from its value
* `MUL` - modifies ACC register by multiplying SRC from its value
* `DIV` - modifies ACC register by dividing SRC from its value
* `DIVI` - modifies ACC register by decimal dividing SRC from its value
* `MOD` - modifies ACC register to remainder of decimal division by SRC from its value
* `NEG` - modifies ACC register to its negate value
* `FLOOR` - modifies ACC register to its floor value
* `ROUND` - modifies ACC register to its rounded value
* `CEIL` - modifies ACC register to its ceil value
* `RND SRC` - modifies ACC register to floating-point random value between 0 (inclusive) and SRC (exclusive)
* `RNDI` - modifies ACC register to decimal random value between 0 (inclusive) and SRC (exclusive)

Logical operations:
* `NOT` - modifies ACC register to logical `NOT` value - any non-zero value results to 0 and zero value results to 1
* `AND SRC` - modifies ACC register to logical `AND` value of ACC register and SRC
* `OR SRC` - modifies ACC register to logical `OR` value of ACC register and SRC
* `XOR SRC` - modifies ACC register to logical `XOR` value of ACC register and SRC

Flow control:
* `JMP label` - sets IP register to instruction label by `label`
* `JZ label` - if ACC register has zero value, sets IP register to instruction label by `label`
* `JNZ label` - if ACC register has non-zero value, sets IP register to instruction label by `label`
* `JG label` - if ACC register has value greater than 0, sets IP register to instruction label by `label`
* `JGE label` - if ACC register has value greater than or equal to 0, sets IP register to instruction label by `label`
* `JL label` - if ACC register has value less than 0, sets IP register to instruction label by `label`
* `JLE label` - if ACC register has value lesser than or equal to 0, sets IP register to instruction label by `label`
* `JNUM label` - if ACC register has number value, sets IP register to instruction label by `label`
* `JNAN label` - if ACC register has not-a-number value, sets IP register to instruction label by `label`
* `CALL label` - pushes actual IP register value to stack and sets IP register to instruction label by `label`
* `RET label` - pops value from stack and sets IP register to it
* `HALT` - sets CPU to waiting state and stops modifying IP register

## Development Guide

### How to Build Locally

Use NodeJS 12+.

Run:
* `yarn`
* `yarn start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### How to Test

Run:
* `yarn test`

Launches the test runner in the interactive watch mode.

### How to Build Production

Run:
* `yarn build`

Builds the app for production to the `build` folder.

### How to Deploy

Source code is automatically built from `master` branch at [Gitlab Pipelines](https://gitlab.com/DavidKaspar/multi-core-cpu-simulator/-/pipelines)
